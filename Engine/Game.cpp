/****************************************************************************************** 
 *	Chili DirectX Framework Version 16.07.20											  *	
 *	Game.cpp																			  *
 *	Copyright 2016 PlanetChili.net <http://www.planetchili.net>							  *
 *																						  *
 *	This file is part of The Chili DirectX Framework.									  *
 *																						  *
 *	The Chili DirectX Framework is free software: you can redistribute it and/or modify	  *
 *	it under the terms of the GNU General Public License as published by				  *
 *	the Free Software Foundation, either version 3 of the License, or					  *
 *	(at your option) any later version.													  *
 *																						  *
 *	The Chili DirectX Framework is distributed in the hope that it will be useful,		  *
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of						  *
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the						  *
 *	GNU General Public License for more details.										  *
 *																						  *
 *	You should have received a copy of the GNU General Public License					  *
 *	along with The Chili DirectX Framework.  If not, see <http://www.gnu.org/licenses/>.  *
 ******************************************************************************************/
#include "MainWindow.h"
#include "Game.h"
#include"Defender.h"
#include"Alien.h"
#include<random>
#include"MotherShip.h"
#include"Meteor.h"
#include"Alien1.h"


Game::Game( MainWindow& wnd )
	:
	wnd( wnd ),
	gfx( wnd ),
	rng(rd()),
	MeteorXdist(0, 800),
	xDist(0,800),
	yDist(0,100),
	vxDist(-2,2),
	vyDist(-2,2)
{

	
	for (int i = 0; i < nAlien; i++)
	{
		alien[i].Init(xDist(rng), yDist(rng), vxDist(rng), vyDist(rng));
	}

	for (int i = 0; i < nAlien1; i++)
	{
		alien1[i].Init(xDist(rng), yDist(rng), vxDist(rng), vyDist(rng));
	}
	
	
	
}



void Game::Go()
{
	gfx.BeginFrame();	
	UpdateModel();
	ComposeFrame();
	gfx.EndFrame();
}

void Game::UpdateModel()

{
	bool allHit = true;


	for (int i = 0; i < nAlien; i++)
	{
		allHit = allHit && alien[i].Hit;
	}

	if (allHit)
	{
		lvl += 1; 
		if (lvl == 2)

		{
			nAlien = 15;
			nAlien1 = 5;

		}
	}
	


	if (lvl == 2)
	{
		if (!initialized)
		{
			for (int i = 0; i < nAlien; i++)
			{
				alien[i].Init(xDist(rng), yDist(rng), vxDist(rng), vyDist(rng));
			}

			for (int i = 0; i < nAlien1; i++)
			{
				alien1[i].Init(xDist(rng), yDist(rng), vxDist(rng), vyDist(rng));
			}
			initialized = true;
		}


	}






	


	for (int i = 0; i < nAlien; i++)
	{
		alien[i].Update();
	}
	for (int i = 0; i < nAlien1; i++)

	{
		alien1[i].Update();
	}


	for (int i = 0; i < nAlien; i++)

	{
		gun.CoalisionAlien(alien[i], def);
	}

	for (int i = 0; i < nAlien1; i++)

	{
		gun.CoalisionAlien1(alien1[i], def);
	}




	meteor[nMeteor].Update();
	def.Coalision(meteor[nMeteor]);
	MS.Update();
	MS.UpdateHealth();
	gun.CoalisionMS(MS,def);
	CometKliker += nComet;

	if (wnd.kbd.KeyIsPressed(VK_LEFT))
	{
		def.x0 =def.x0 - def.speed;

	}


	if (wnd.kbd.KeyIsPressed(VK_RIGHT))
	{
		def.x0 +=def.speed;

	}

	if (wnd.kbd.KeyIsPressed(VK_UP))
	{
		def.y0 -= def.speed;

	}
	if (wnd.kbd.KeyIsPressed(VK_DOWN))
	{
		def.y0 += def.speed;

	}

	def.ClampScreen();

	

	
	if (AmunitionMax > 0)
	{
		if (wnd.kbd.KeyIsPressed(VK_SPACE))
		{

			if (inhibitSpace == false)
		{
			AmunitionMax -= Bullet;
			
				gun.Draw(gfx, def,MS);
			 
		}

		}
	
	}
	
	inhibitSpace = (wnd.kbd.KeyIsPressed(VK_SPACE));
}

void Game::ComposeFrame()

{
	if (CometKliker < 100)

	{
		meteor[nMeteor].x0 = MeteorXdist(rng);
	}




	if (CometKliker >100)
	{
		meteor[nMeteor].Draw(gfx);
		meteor[nMeteor].vx = 3;
		meteor[nMeteor].vy = 5;
	}



	if (meteor[nMeteor].y0 + meteor[nMeteor].height >= gfx.ScreenHeight - 2)
	{
		CometKliker = 0;
		meteor[nMeteor].Reset();

	}




	MS.Draw(gfx);
	MS.DrawHealth(gfx);


	if (HealthBar > 0)
	{
		def.DrawDefender(gfx);
	}


	for (int i = 0; i < nAlien; i++)
	{
		alien[i].Draw(gfx);

	}
	for (int i = 0; i < nAlien1; i++)
	{
		alien1[i].Draw(gfx);
	}
	

	for (int x = 0; x < AmunitionMax; x++)
	{
		for (int y = 0; y < 5; y++)
		{
			gfx.PutPixel(x, y, Colors::Magenta);
		}

	}

	for (int x = 0; x < HealthBar; x++)
	{
		for (int y = 5; y < 10; y++)
		{
			gfx.PutPixel(x, y, Colors::Green);
		}

	}

	if (def.HitComet)
	{
		Inhibit = 1;
		HealthBar -= DMG;
		InhibitDMG +=Inhibit;
		def.HitComet = false;
	}

	if (InhibitDMG>1)

	{

		DMG = 0;
	if (InhibitDMG > 20)

	{
		InhibitDMG = 0;
		Inhibit = 0;
		DMG = 25;

	}
	}


}
