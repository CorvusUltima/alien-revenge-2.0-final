#include "Alien1.h"

void Alien1::Draw(Graphics & gfx)
{
	if(!Hit1) 
	{

	for (int y = y1; y < height1 + y1; ++y)
	{
		for (int x = x1; x < width1 + x1; ++x)

		{
			gfx.PutPixel(x, y, Colors::Yellow);
		}
	}
	}
}

void Alien1::Init(int in_x, int in_y, int in_vx, int in_vy)
{
	x1 = in_x;
	y1 = in_y;
	vx1 = in_vx;
	vy1= in_vy;
}

void Alien1::Update()
{

	x1 += vx1;
	y1 += vy1;

	const int right = x1 + width1;
	if (x1 < 0)
	{
		x1 = 0;
		vx1 = -vx1;
	}
	else if (right >= Graphics::ScreenWidth)
	{
		x1 = (Graphics::ScreenWidth - 1) - width1;
		vx1 = -vx1;
	}

	const int bottom = y1 + height1;
	if (y1 <10)
	{
		y1 = 10;
		vy1 = -vy1;
	}
	else if (bottom >= Graphics::ScreenHeight)
	{
		y1 = (Graphics::ScreenHeight - 1) - height1;
		vy1 = -vy1;
	}

}
