/****************************************************************************************** 
 *	Chili DirectX Framework Version 16.07.20											  *	
 *	Game.h																				  *
 *	Copyright 2016 PlanetChili.net <http://www.planetchili.net>							  *
 *																						  *
 *	This file is part of The Chili DirectX Framework.									  *
 *																						  *
 *	The Chili DirectX Framework is free software: you can redistribute it and/or modify	  *
 *	it under the terms of the GNU General Public License as published by				  *
 *	the Free Software Foundation, either version 3 of the License, or					  *
 *	(at your option) any later version.													  *
 *																						  *
 *	The Chili DirectX Framework is distributed in the hope that it will be useful,		  *
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of						  *
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the						  *
 *	GNU General Public License for more details.										  *
 *																						  *
 *	You should have received a copy of the GNU General Public License					  *
 *	along with The Chili DirectX Framework.  If not, see <http://www.gnu.org/licenses/>.  *
 ******************************************************************************************/
#pragma once

#include "Keyboard.h"
#include "Mouse.h"
#include "Graphics.h"
#include"Defender.h"
#include"Gun.h"
#include"Alien.h"
#include<random>
#include"MotherShip.h"
#include"Meteor.h"
#include"Alien1.h"

class Game
{
public:
	Game( class MainWindow& wnd );
	Game( const Game& ) = delete;
	Game& operator=( const Game& ) = delete;
	void Go();
private:
	void ComposeFrame();
	void UpdateModel();
	/********************************/
	/*  User Functions              */
	/********************************/
private:
	MainWindow& wnd;
	Graphics gfx;
	Defender def;
	Gun gun;

	/********************************/
	/*  User Variables              */
	/********************************/

	std::random_device rd;
	std::mt19937 rng;
	std::uniform_int_distribution<int> MeteorXdist;
	std::uniform_int_distribution<int> xDist;
	std::uniform_int_distribution<int> yDist;
	std::uniform_int_distribution<int> vxDist;
	std::uniform_int_distribution<int> vyDist;

	int CometKliker = 0;
	int nComet = 1;
	const static int nAlienMax = 1000;
	const static int nMeteorMax = 1000;
	const static int nAlien1Max = 1000;
	int nAlien = 5 ;
	int nAlien1 = 0;
	int nMeteor =1;
	int DMG = 25;
	Alien alien[nAlienMax];
	Alien1 alien1[nAlien1Max];
	MotherShip	MS;
	Meteor meteor[nMeteorMax];
	int AmunitionMax = gfx.ScreenWidth - 1;
	int HealthBar = gfx.ScreenWidth - 1;
	int Bullet = 10;
	bool inhibitSpace = false;
	bool AlienHit = false;
	bool resetKomet = false;
	int  InhibitDMG = 0;
	int Inhibit = 1;
	int lvl = 1; 
	bool initialized = false;
};