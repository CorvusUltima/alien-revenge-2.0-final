#include "Meteor.h"

void Meteor::Update()
{
	x0 += vx;
	y0 += vy;

	const int right = x0 + width;
	if (x0 < 0)
	{
		x0 = 0;
		vx = -vx;
	}
	else if (right >= Graphics::ScreenWidth)
	{
		x0 = (Graphics::ScreenWidth - 1) - width;
		vx = -vx;
	}

	const int bottom = y0 + height;
	if (y0 <10)
	{
		y0 = 10;
		vy = -vy;
	}
	else if (bottom >= Graphics::ScreenHeight)
	{
		y0 = (Graphics::ScreenHeight - 1) - height;
		vy = -vy;
	}

}

void Meteor::Init(int in_x, int in_y, int in_vx, int in_vy)
{

	x0 = in_x;
	y0 = in_y;
	vx = in_vx;
	vy = in_vy;


}

void Meteor::Draw(Graphics & gfx)
{

	for (int y = y0; y < height + y0; ++y)
	{
		for (int x = x0; x < width + x0; ++x)

		{
			gfx.PutPixel(x, y, 255, 0, 0);
		}

	}


}

void Meteor::Reset()
{
	int x0 = 400;
	int y0 = 0;
	int height = 0;
	int width =0;
}
