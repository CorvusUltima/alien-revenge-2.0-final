#include "PowerUp.h"
#include"Graphics.h"

void Health::UpdateHealth()
{
	
	xH += vxH;
	yH += vyH;

	const int right = xH + widthH;
	if (xH < 0)
	{
		xH = 0;
		vxH = -vxH;
	}
	else if (right >= Graphics::ScreenWidth)
	{
		xH = (Graphics::ScreenWidth - 1) - widthH;
		vxH = -vxH;
	}

	const int bottom = yH + heightH;
	if (yH < 10)
	{
		yH = 10;
		vyH = -vyH;
	}
	else if (bottom >= Graphics::ScreenHeight)
	{
		yH = (Graphics::ScreenHeight - 1) - heightH;
		vyH = -vyH;
	}



}

void Health::DrawHealth(Graphics& gfx)
{
	for (int y = yH; y < heightH + yH; ++y)
	{
		for (int x = xH; x < widthH + xH; ++x)

		{
			gfx.PutPixel(x, y, 0, 255, 0);
		}
	}


}

void Health::UpdateAmmo()
{

	xA += vxA;
	yA += vyA;

	const int right = xA+ widthA;
	if (xA < 0)
	{
		xA = 0;
		vxA = -vxA;
	}
	else if (right >= Graphics::ScreenWidth)
	{
		xA= (Graphics::ScreenWidth - 1) - widthA;
		vxA = -vxA;
	}

	const int bottom = yA+ heightA;
	if (yA < 10)
	{
		yA= 10;
		vyA = -vyA;
	}
	else if (bottom >= Graphics::ScreenHeight)
	{
		yA = (Graphics::ScreenHeight - 1) - heightA;
		vyA = -vyA;
	}


}

void Health::DrawAmmo(Graphics& gfx)
{
	for (int y = yA; y < heightA + yA; ++y)
	{
		for (int x = xA; x < widthA + xA; ++x)

		{
			gfx.PutPixel(x, y, 0, 255, 0);
		}
	}
}
