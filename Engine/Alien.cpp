#include "Alien.h"
#include<random>

void Alien::Draw(Graphics & gfx)
{
	if (!Hit)
	{
	for (int y = y0; y < height+ y0; ++y)
	{
		for (int x = x0; x < width + x0; ++x)

		{
			gfx.PutPixel(x, y, 0, 255, 0);
		}
	}
	}
	
}

void Alien::Init(int in_x, int in_y, int in_vx, int in_vy)
{
	x0 = in_x;
	y0 = in_y;
	vx = in_vx;
	vy = in_vy;
}

void Alien::Update()
{

	x0 += vx;
	y0 += vy;

	const int right = x0 +width;
	if (x0 < 0)
	{
		x0 = 0;
		vx = -vx;
	}
	else if (right >= Graphics::ScreenWidth)
	{
		x0 = (Graphics::ScreenWidth - 1) - width;
		vx = -vx;
	}

	const int bottom = y0 + height;
	if (y0 <10)
	{
		y0 = 10;
		vy = -vy;
	}
	else if (bottom >= Graphics::ScreenHeight)
	{
		y0 = (Graphics::ScreenHeight - 1) - height;
		vy = -vy;
	}



}

/*void Alien::Respawn()
{

	std::random_device rd();
	std::mt19937 rng(rd());
	std::uniform_int_distribution<int> xDist(0, 750);
	std::uniform_int_distribution<int> yDist(0, 550);
	std::uniform_int_distribution<int> vxDist(-2, 2);
	std::uniform_int_distribution<int> vyDist(-2, 2);


	Init(xDist(rng), yDist(rng), vxDist(rng), vyDist(rng));
	Hit = false;
}*/
